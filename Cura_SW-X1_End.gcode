M117 Print job complete!
G91 ; Relative positioning
G1 Z1.0 F3000 ; Move up Z to prevent scratching print bed
G90 ; Absolute positioning
;G1 X0 Y200 F1000 ; Prepare for part removal
M104 S0; Turn off extruder hotend
M140 S0 ; Turn off print bed heater
M117 Preparing part for removal...
G1 X0 Y300 F2000 ; Prepare for part removal
;G1 Y275 ; (From Octoprint)
M84 ; Disable motors
M106 S0 ; Turn off fans
M117 Done!
