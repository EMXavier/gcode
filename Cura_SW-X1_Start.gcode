M117 Heating print bed...
M900 K0 ; Disable Linear Advance Factor
M140 S{material_bed_temperature_layer_0} ; Set bed temperature
M190 S{material_bed_temperature_layer_0} ; Wait for bed to warm up
M117 Preheating extruder...
M104 S160 ; Preheat extruder to 160C
G28 ; Home all axes
M117 Auto-leveling print bed...
G29 ; Auto level print bed (BL-Touch)
G92 E0 ; Reset extruder
M117 Brining extruder up to temp...
M104 S{material_print_temperature_layer_0} ; Set extruder temperature
M109 S{material_print_temperature_layer_0} ; Wait for temperature
G1 Z2.0 F3000 ; Move up Z to prevent scratching print bed
G1 X0.1 Y20 Z0.3 F5000.0 ; Move to start-line position
M117 Purging hotend...
G1 X0.1 Y200.0 Z0.3 F1500.0 E15 ; Draw 1st line
G1 X0.4 Y200.0 Z0.3 F5000.0 ; Move to side a little
G1 X0.4 Y20 Z0.3 F1500.0 E30 ; Draw 2nd line
G92 E0 ; Reset extruder
G1 Z2.0 F3000 ; Move up Z to prevent scratching print bed
G1 X5 Y20 Z0.3 F5000.0 ; Move over to prevent blob squish
M117 Printing...
