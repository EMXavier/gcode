; Disable motors
M84

; Disable all heaters
{% snippet 'disable_hotends' %}
{% snippet 'disable_bed' %}

; Disable fans
M106 S0

; Raise a bit + retract filament
G1 Z+50 E-0.8 F4500
