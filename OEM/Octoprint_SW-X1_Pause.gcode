{% if pause_position.x is not none %}
; Relative XYZE
G91
M83

; Retract filament of 0.8 mm up, move Z slightly upwards and
G1 Z+5 E-0.8 F4500

; Absolute XYZE
M82
G90

; Move to a safe rest position, adjust as necessary
G1 X0 Y0
{% endif %}
