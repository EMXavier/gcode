{% if pause_position.x is not none %}
; Relative extruder
M83

; Prime nozzle
G1 E-0.8 F4500
G1 E0.8 F4500
G1 E0.8 F4500

; absolute E
M82

; absolute XYZ
G90

; Reset E
G92 E{{ pause_position.e }}

; WARNING!!! - Use M83 or M82(extruder absolute mode) according what your slicer generates
M83 ; Extruder relative mode

; Move back to pause position XYZ
G1 X{{ pause_position.x }} Y{{ pause_position.y }} Z{{ pause_position.z }} F4500

; Reset to feed rate before pause if available
{% if pause_position.f is not none %}G1 F{{ pause_position.f }}{% endif %}
{% endif %}
