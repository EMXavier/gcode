M117 Pausing print job...
G91                ; Relative movement
G1 Z5              ; Move head away vertically
G1 E-6 F1000       ; Relieve filament pressure
G90                ; Absolute position ON
G1 Y-100           ; Move to front position
G91                ; Relative movement
M117 Retracting filament for change...
G1 E-80 F1000      ; Retract filament for swap
G90                ; Absolute position ON
