M900 K0                                     ; SET LA to 0
M104 S150                                   ; Start heating extruder
M140 S{material_bed_temperature_layer_0}    ; Heatbed temperature
M190 S{material_bed_temperature_layer_0}    ; Wait for bed to heat
G4 S60                                      ; Wait 1 more min for heat
M83                                         ; Extruder relative mode
G28                                         ; HOME all axes
G29                                         ; Bed autolevel
G92 E0                                      ; RESET extruder
G1 X0 Y0 F5000                              ; MOVE to 0/0/0
G1 Z0
M109 S{material_print_temperature_layer_0}  ; Heat up extruder
M42 P4 S0                                   ; Turn off LED
M42 P5 S0
M42 P6 S0
G1 X20 Y5 Z0.3 F5000.0                      ; MOVE to start position
G1 Z0.3 F1000                               ; Print height
G1 X200 Y5 F1500.0 E15                      ; Draw 1st line
G1 X200 Y5.3 Z0.3 F5000.0                   ; MOVE to side a bit
G1 X5.3  Y5.3 Z0.3 F1500.0 E30              ; Draw 2nd line
M82                                         ; Extruder absolute mode
G92 E0
