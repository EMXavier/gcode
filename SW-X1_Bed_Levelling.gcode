M140 S60 ; Starting by heating the bed for nominal mesh accuracy
M117 Homing all axes... ; Send message to printer display
G28      ; Home all axes
M117 Heating the bed... ; Send message to printer display
M190 S60 ; Waiting until the bed is fully warmed up
M300 S1000 P500 ; Chirp to indicate bed mesh levels is initializing
M117 Creating bed mesh levels... ; Send message to printer display
M155 S30 ; Reduce temperature reporting rate to reduce output pollution
@BEDLEVELVISUALIZER	; Tell the plugin to watch for reported mesh
G29 T	   ; Run bilinear probing
M155 S3  ; Reset temperature reporting
M140 S0 ; Cooling down the bed
M300 S440 P200 ; Make calibration completed tones
M300 S660 P250
M300 S880 P300
M117 Bed mesh levels complete! ; Send message to printer display
